<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="product" content="Metro UI CSS Framework">
    <meta name="description" content="Simple responsive css framework">
    <meta name="author" content="RezaBuyungNalendra">

    <link href="style/docs/css/metro-bootstrap.css" rel="stylesheet">
    <link href="style/docs/css/metro-bootstrap-responsive.css" rel="stylesheet">
    <link href="style/docs/css/iconFont.css" rel="stylesheet">
    <link href="style/docs/css/docs.css" rel="stylesheet">
    <link href="style/docs/js/prettify/prettify.css" rel="stylesheet">
    <link href="style/bxslider/jquery.bxslider.css" rel="stylesheet">

    <!-- Load JavaScript Libraries -->
    <script src="style/docs/js/jquery/jquery.min.js"></script>
    <script src="style/docs/js/jquery/jquery.widget.min.js"></script>
    <script src="style/docs/js/jquery/jquery.mousewheel.js"></script>
    <script src="style/docs/js/prettify/prettify.js"></script>
    <script src="style/docs/js/holder/holder.js"></script>
    <script src="style/bxslider/jquery.bxslider.min.js"></script>
    
    <!-- Metro UI CSS JavaScript plugins -->
    <script src="style/docs/js/load-metro.js"></script>

    <!-- Local JavaScript -->
    <script src="style/docs/js/docs.js"></script>
    <script src="style/docs/js/github.info.js"></script>
    <script src="style/docs/js/marquee.js"></script>
    <?php 
        include ("config/config.php");
    ?>

    <script>
        $(document).ready(function(){
           $("tr:even").addClass("even");
           $("tr:odd").addClass("odd");
           
           setInterval("location.reload(true)", 900000);
           
        $(function(){
        $('div.inner_area marquee').marquee('smooth_m').mouseover(function () {
          $(this).trigger('start');
        }).mouseout(function () {
          $(this).trigger('start');
        }).mousemove(function (event) {
          if ($(this).data('drag') == true) {
            this.scrollLeft = $(this).data('scrollX') + ($(this).data('x') - event.clientX);
          }
        }).mousedown(function (event) {
          $(this).data('drag', true).data('x', event.clientX).data('scrollX', this.scrollLeft);
        }).mouseup(function () {
          $(this).data('drag', false);
        });
        });

    </script>
    
    <style type="text/css">
        body{background-image: url("style/img/bg.jpg");background-size: cover;background-repeat: no-repeat;}
        #lg-atas{margin: 10px 0;overflow: hidden;}
        /*.head {float:right;margin-right:40%;}*/
            .head h3{color: #f0f0f0;}
            .head h5{color: #f0f0f0;}
        .logo{width: 90px;}
        .footer{position:absolute;bottom:0;width:100%;text-align: center;background-color: black;padding-top: 9px;}
        .footer p{font-size: 10px;line-height:15px;color: darkgrey;}
        .info-news p{font-size: 14px;color: #333333;}
        .info-news img{margin: 10px 0 20px 0;}
        .info-p {display: block;padding: 10px;background-color: rgba(0, 0, 0, .5);}
        .info-p p{font-size: 14px;color: #dedbdb;}
        .spans {width: 220px;margin-right: 5px;}
        .hd-btm {text-align: center;padding-top: 10px;}
        #head-info {display: block;background-color: red;padding: 10px;}
        /*#img-news {overflow:hidden;width: 400px;margin:auto;text-align: center;background-color: red;}*/
    </style>

    <title>Agenda Ruang Rapat</title>
</head>
<?php
    //query agenda rapat
    $agd = "SELECT A . * , (SELECT COUNT( room_name ) FROM today_v3 WHERE room_name = A.room_name) AS jumlah FROM today_v3 A";
    $agd_data = mysql_query($agd,$db);
    while ($row_agd = mysql_fetch_array($agd_data)){$hasil[] = $row_agd;}
    
?>
<?php
    //query news biro umum
    $sql_brumum = "SELECT * FROM tbl_news WHERE owner = '4'";
    $data_brumum = mysql_query($sql_brumum,$db);
    while ($row_brumum = mysql_fetch_array($data_brumum)){$rslt_brumum[] = $row_brumum;}
    //query news biro sdm
    $sql_brsdm = "SELECT * FROM tbl_news WHERE owner = '3'";
    $data_brsdm = mysql_query($sql_brsdm,$db);
    while ($row_brsdm = mysql_fetch_array($data_brsdm)){$rslt_brsdm[] = $row_brsdm;}
    //query news biro humas
    $sql_brhm = "SELECT * FROM tbl_news WHERE owner = '2'";
    $data_brhm = mysql_query($sql_brhm,$db);
    while ($row_brhm = mysql_fetch_array($data_brhm)){$rslt_brhm[] = $row_brhm;}
    //query news koperasi pjp
    $sql_pjp = "SELECT * FROM tbl_news WHERE owner = '5'";
    $data_pjp = mysql_query($sql_pjp,$db);
    while ($row_pjp = mysql_fetch_array($data_pjp)){$rslt_pjp[] = $row_pjp;}
    // var_dump($data_pjp);
    //query all news
    $y = date('Y'); $m = date('m'); $d = date('d');
    $time = mktime(0, 0, 0, $m, $d, $y);
    $current_date = date('Y-m-d');
    $sql_all = "SELECT * FROM tbl_news WHERE tayang >= $time ORDER BY tayang DESC ";
    $data_all = mysql_query($sql_all,$db);
    while ($row_all = mysql_fetch_array($data_all)){$rslt_all[] = $row_all;}
?>

<body class="metro" style="width:100%;">
    <div class="container">
        <?php
            
            $array_hr= array(1=>"Senin","Selasa","Rabu","Kamis","Jumat","Sabtu","Minggu");
            $hr = $array_hr[date('N')];
            $tgl= date('j');
            $array_bln = array(1=>"Januari","Februari","Maret", "April", "Mei","Juni","Juli","Agustus","September","Oktober", "November","Desember");
            $bln = $array_bln[date('n')];
            $thn = date('Y');
        ?>
        <div class="grid" style="margin: 0 auto;width: 1366px;">
            <div class="row">
            <div style="margin: 0 10px;width:900px;float:left;">
            <div style="overflow:hidden;">
                <div class="head">
                    <h3>AGENDA RAPAT HARI INI</h3>
                    <h5><?php echo $hr.", ".$tgl." ".$bln." ".$thn."";?></h5>
                </div>
                <div id="lg-atas" style="float:right;">
                    <!-- <div class="logo"><img src="assets/img/logo_biro_umum.png"></div> -->
                </div>
                <div id="lg-atas" style="float:left;">
                    <div class="times inverse" data-role="times" data-blink="false" style="text-align:left"></div>
                </div>
            </div>
            
        
                <div class="example" style="height:800px;">
                    <table class="table" style="background-color: rgba(255, 255, 255, .6);">
                        <thead>
                        <tr>
                            <th class="text-left" width="300px">Waktu</th>
                            <th class="text-left" width="370px;">Rapat</th>
                            <th class="text-left">PIC</th>
                        </tr>
                        </thead>
                    </table>
                    <marquee class="smooth_m" behavior="scroll" direction="up" onmouseout="this.start()" onmouseover="this.stop()" scrollamount="2" height="720">
                    <table class="table striped">

                        <tbody>
                        <?php
                            $no = 1; $jml = 1;
                            foreach ($hasil as $row_agd){
                                if($row_agd['room_name'] == 'SG-1&2'):
                                    $gedung = '(Gedung Baru Lantai Dasar)';
                                    // $clr = 'bg-darkBlue';
                                elseif($row_agd['room_name'] == 'SG-3'):
                                    $gedung = '(Gedung Baru Lantai Dasar)';
                                    // $clr = 'bg-magenta';
                                elseif($row_agd['room_name'] == 'SG-4'):
                                    $gedung = '(Gedung Baru Lantai Dasar)';
                                    // $clr = 'bg-lightBlue';
                                elseif($row_agd['room_name'] == 'SG-5'):
                                    $gedung = '(Gedung Baru Lantai Dasar)';
                                    // $clr = 'bg-emerald';
                                elseif($row_agd['room_name'] == 'R-203'):
                                    $gedung = '(Gedung Madiun Lantai 3)';
                                    // $clr = 'bg-amber';
                                elseif($row_agd['room_name'] == 'R-204'):
                                    $gedung = '(Gedung Madiun Lantai 4)';
                                    // $clr = 'bg-crimson';
                                elseif($row_agd['room_name'] == 'SS-1&2'):
                                    $gedung = '(Gedung TS 2A Lantai 2)';
                                    // $clr = 'bg-darkBlue';
                                elseif($row_agd['room_name'] == 'SS-3'):
                                    $gedung = '(Gedung TS 2A Lantai 2)';
                                    // $clr = 'bg-magenta';
                                elseif($row_agd['room_name'] == 'SS-4'):
                                    $gedung = '(Gedung TS 2A Lantai 2)';
                                    // $clr = 'bg-lightBlue';
                                //tambahan di tahun 2015
                                    $gedung = '(Gedung TS 2A Lantai 2)';
                                    elseif($row['room_name'] == 'RR-Utama1'):
                                        $gedung = '(Gedung Utama Lantai 2)';
                                    elseif($row['room_name'] == 'RR-Utama2'):
                                        $gedung = '(Gedung Utama Lantai 2)';
                                    elseif($row['room_name'] == 'RR-Prok1'):
                                        $gedung = '(Gedung Pusbindiklatren Lantai 1)';
                                    elseif($row['room_name'] == 'RR-Prok2'):
                                        $gedung = '(Gedung Pusbindiklatren Lantai 1)';
                                    elseif($row['room_name'] == 'RR-Rasuna1'):
                                        $gedung = '(Gedung Wisma Bakrie 2 Lantai 6)';
                                    elseif($row['room_name'] == 'RR-Rasuna2'):
                                        $gedung = '(Gedung Wisma Bakrie 2 Lantai 6)';
                                                            // $clr = 'bg-amber';
                                endif;

                                if ($jml <= 1){
                        ?>
                            <tr class="fg-white bg-darkBlue"><td colspan="2"><i class="icon-location fg-white"></i> <?=$row_agd['room_name'];echo "-";echo $gedung;?><td></tr>
                        <?php
                            $jml = $row_agd['jumlah'];
                            $no++;
                            } else {
                            $jml = $jml - 1;
                            }
                        ?>
                                    <tr><td class="text-left" width="140px;"><?=date('H:i',$row_agd['start_time']);?> s/d <?=date('H:i',$row_agd['end_time']);?></td>
                                        <td class="text-center"><?=$row_agd['name'];?></td>
                                        <td class="text-center">
                                            <div><?=$row_agd['penanggung_jawab'];?></div>
                                            <div><i style="font-size:8px;"><?=$row_agd['unit']; echo " / <strong>".$row_agd['telp']."</strong>";?></i></div>
                                        </td>
                                    </tr>
                        <?php } ?>
                        </tbody>

                        <br>
                    </table>
                    </marquee>
                </div>
                
            </div>
            
            <div style="float:left;padding:0 5px;width:440px">
                <!-- <div style="margin:20px 10px 24px 290px;"><button class="button" id="createFlatWindow">login</button> | <i class="icon-user" style="background: black;color: white;padding: 5px;border-radius: 50%"></i></div> -->
                <div style="margin:20px 10px 24px 30px;"><img style="width:217px" src="./style/img/logo.png"><a href="#" id="createFlatWindow"><img style="width:130px;" src="./style/img/logo_biro_umum.png"> | <i class="icon-user" style="background: black;color: white;padding: 5px;"></i></a></div>

                <!-- <div class="span3"> -->
                    <div class="example-info">
                    <!--<div class="tile double live" data-role="live-tile" data-effect="slideDown" style="width:100%;height:200px;">
                        <?php if (empty($rslt_all)){
                            echo "no data";
                        } else { 
                            foreach ($rslt_all as $row_all) { ?>
                        <div class="tile-content image image-container" style="width:100%;height:200px;">
                            <img src="./filegambar/<?= $row_all['gambar'];?>">
                            <div class="overlay bg-tile">
                                <?=$row_all['judul'];?>
                            </div>
                        </div>
                        <?php }} ?>
                    </div>-->

                    <div style="padding :10px;">
                        <div id='head-info' class="bg-teal">
                            <h2 class="fg-white">Pengumuman Bappenas <i class="icon-arrow-right-3 on-right"></i></h2>
                        </div>
                    <marquee class="smooth_m" behavior="scroll" direction="up" onmouseout="this.start()" onmouseover="this.stop()" scrollamount="4" height="710">
                        <?php if (empty($rslt_all)){
                            echo "no data";
                        } else {
                            foreach ($rslt_all as $row_all) { ?>
                        <div class="info-news">
                            <span class="info-p">
                                <span class='icon-bookmark-3 fg-yellow'><i class='fg-yellow'> <?=$row_all['owner'];?></i></span>
                                <p><?=$row_all['judul'];?></p>
                                <span>
                                    <i class='fg-yellow' style='font-size:10px'> 
                                        <?php
                                            $date = substr($row_all['date'],0,10);
                                            list($y,$m,$d) = explode('-', $date);
                                            $tgl = $d."-".$m."-".$y;
                                            echo $tgl;
                                        ?>
                                    </i>
                                </span>
                            </span>
                            <p><?=$row_all['deskripsi'];?></p>
                            <span id='img-news'><img src="./filegambar/<?=$row_all['gambar'];?>"></span>
                        </div>
                        <br>
                        <?php }} ?>
                    </marquee>
                    </div>

                    </div>
                <!-- </div> -->
            </div>
            
        </div>
        </div>

        <div class="footer">
            <p>
                Developed by Pusdatinrenbang |
                http://agenda.bappenas.go.id |
                copyright &copy; 2015 All Rights Reserved</p>
        </div>
    </div> <!-- End of container -->

    <script src="assets/docs/js/hitua.js"></script>
    <script type="text/javascript">
        $('.bxslider').bxSlider({
          // mode: 'vertical',
          auto: true,
          autoControls: true,
          adaptiveHeight: true,
          captions: true
        });
        $("#createFlatWindow").on('click', function(){
            $.Dialog({
                overlay: true,
                shadow: true,
                flat: true,
                draggable: true,
                icon: '<img src="images/excel2013icon.png">',
                title: 'Flat window',
                content: '',
                padding: 10,
                onShow: function(_dialog){
                    var content = '<form class="user-input" action="http://localhost:8888/info/info_manager/index.php/login/" method="post" enctype="multipart/form-data">' +
                            '<label>Login</label>' +
                            '<div class="input-control text"><input type="text" name="login"><button class="btn-clear"></button></div>' +
                            '<label>Password</label>'+
                            '<div class="input-control password"><input type="password" name="password"><button class="btn-reveal"></button></div>' +
                            '<div class="input-control checkbox"><label><input type="checkbox" name="c1" checked/><span class="check"></span>Rememmber me</label></div>'+
                            '<div class="form-actions">' +
                            '<button type="submit" name="submit" class="button primary">Login</button>&nbsp;'+
                            '<button class="button" type="button" onclick="$.Dialog.close()">Cancel</button> '+
                            '</div>'+
                          
                    $.Dialog.title("User login");
                    $.Dialog.content(content);
                }
               });
        });        
    </script>
</body>
</html>