<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {
    public function __construct(){
        parent::__construct();
        
    }
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */

    public function index()
    {
        $sessionData = array();
        if (isset($_POST['submit'])){
            $username = $this->input->post('username',TRUE);
            $password = MD5($this->input->post('password',TRUE));
            $this->db->where('username',$username);
            $this->db->where('password',$password);
            $data = $this->db->get('tbl_login');
            // echo "ok";
            // exit();
            if ($data->num_rows()>0){
                foreach ($data->result() as $row) {
                    $sessionData['username'] = $row->username;
                    $sessionData['role'] = $row->role;

                    //Fungsi Tracking
                    $log = array(
                        'user_name' => $row->username,
                        'tipe' => "login"
                    );

                    $this->load->model('modeltracking');
                    $this->modeltracking->track($log);

                    $this->session->set_userdata($sessionData);
                    redirect('main/');
                }

            } else {
                redirect('login/index/error');
            }
        }
        $this->load->view('login',$data);

    }

    public function logout(){
        //Fungsi Tracking
        $log = array(
            'user_name' => $this->session->userdata('username'),
            'tipe' => "logout"
        );
        $this->load->model('modeltracking');
        $this->modeltracking->track($log);
        
        $this->session->sess_destroy();
        redirect('login/index/logout');

    }
}