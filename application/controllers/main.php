<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller {
	public function __construct(){
        parent::__construct();

        if($this->session->userdata('username') == ""){
            redirect('login/');
        }
    }

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->model('modView');
		$this->load->model('modInputdata');
		$data['listdata'] = $this->modView->listdata();
		$data['listuser'] = $this->modView->listuser();
		$data['listuser_by'] = $this->modView->listuser_by();
			
		if (isset($_POST['submit'])){
			$this->modInputdata->insertdata();
			redirect('main/index/success');
		}
		$this->load->view('main',$data);
	}

	function edit()
	{
		$this->load->model('modEditdata');
		$this->load->model('modView');
		$this->load->model('modInputdata');
		$data['listdata'] = $this->modView->listdata();
		$data['listuser'] = $this->modView->listuser();
		$data['listuser_by'] = $this->modView->listuser_by();
		if (isset($_POST['edit'])){
			$this->modEditdata->editdata();
			redirect('main/index/success');
		}
		$data['id'] = $this->uri->segment(3);
		$data['rowData'] = $this->modEditdata->viewdata($data['id']);
		$this->load->view('main_edit',$data);
	}

	function delete()
	{
		$this->load->model('modEditdata');
		$data['id'] = $this->uri->segment(3);
		$this->modEditdata->deletedata($data['id']);
		redirect ('main/index/delete_success');

	}

	function tambah(){
		$this->load->model('modelUser');
		$data['userlist'] = $this->modelUser->get_user();
		$this->load->view('input',$data);
	}

	function add_user(){
		$this->load->model('modInputdata');
		$this->modInputdata->add_user();
		redirect ('main/tambah/success');
	}

	function edit_user($id){
        $this->load->model('modelUser');
        $data['edit'] = $this->modelUser->get_edit_user($id);
		$this->load->model('modelUser');
		$data['userlist'] = $this->modelUser->get_user();
        $this->load->view('input', $data);
	}

	function update_user(){
		$id = $this->input->post('id');
		if ($_POST['password'] != ''){
			$pass = md5($_POST['password']);
		} else {
			$pass = $_POST['pwd'];
		}
		$update = array(
			'username' => $this->input->post('username'),
			'password' => $pass,
			'role'	   => $this->input->post('role')
		);
        $this->load->model('modelUser');
		$this->modelUser->edit_user($id, $update);

		redirect('main/index/edit_success');
	}

	function delete_user(){
		
		$id = $this->uri->segment(3);
		$this->load->model('modelUser');
        $this->modelUser->delete($id);
        redirect('main/tambah/delete_success');
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */