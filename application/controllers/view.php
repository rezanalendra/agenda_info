<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class View extends CI_Controller {

	function index()
	{
		$this->load->model('modView');
		$data['listhumas'] = $this->modView->listhumas();
		$data['listsdm'] = $this->modView->listsdm();
		$data['listumum'] = $this->modView->listumum();
		$data['listpjp'] = $this->modView->listpjp();
		$data['listall'] = $this->modView->listall();
		$this->load->view('acara_hari_ini',$data);
	}
	
}