<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="product" content="Metro UI CSS Framework">
    <meta name="description" content="Simple responsive css framework">
    <meta name="author" content="RezaBuyungNalendra">

    <link href="<?=base_url();?>docs/css/metro-bootstrap-responsive.css" rel="stylesheet">
    <link href="<?=base_url();?>docs/css/metro-bootstrap.css" rel="stylesheet">
    <link href="<?=base_url();?>docs/css/iconFont.css" rel="stylesheet">
    <link href="<?=base_url();?>docs/css/docs.css" rel="stylesheet">
    <link href="<?=base_url();?>docs/js/prettify/prettify.css" rel="stylesheet">

    <!-- Load JavaScript Libraries -->
    <script src="<?=base_url();?>docs/js/jquery/jquery.min.js"></script>
    <script src="<?=base_url();?>docs/js/jquery/jquery.widget.min.js"></script>
    <script src="<?=base_url();?>docs/js/jquery/jquery.mousewheel.js"></script>
    <script src="<?=base_url();?>docs/js/prettify/prettify.js"></script>
    <script src="<?=base_url();?>docs/js/holder/holder.js"></script>

    <!-- Metro UI CSS JavaScript plugins -->
    <script src="<?=base_url();?>docs/js/load-metro.js"></script>

    <!-- Local JavaScript -->
    <script src="<?=base_url();?>docs/js/docs.js"></script>
    <script src="<?=base_url();?>docs/js/github.info.js"></script>

    <script src="<?=base_url();?>docs/js/marquee.js"></script>
    <?php 
        $base = 'http://localhost:8888/info_manager/filegambar';
        include ("config/config.php");
    ?>

    <script>
        $(document).ready(function(){
           $("tr:even").addClass("even");
           $("tr:odd").addClass("odd");
           
           setInterval("location.reload(true)", 900000);
           
        $(function(){
        $('div.inner_area marquee').marquee('smooth_m').mouseover(function () {
          $(this).trigger('start');
        }).mouseout(function () {
          $(this).trigger('start');
        }).mousemove(function (event) {
          if ($(this).data('drag') == true) {
            this.scrollLeft = $(this).data('scrollX') + ($(this).data('x') - event.clientX);
          }
        }).mousedown(function (event) {
          $(this).data('drag', true).data('x', event.clientX).data('scrollX', this.scrollLeft);
        }).mouseup(function () {
          $(this).data('drag', false);
        });
        });

    </script>
    
    <style type="text/css">
        body{background-image: url("../style/img/bg.jpg");background-size: cover;background-repeat: no-repeat;}
        #lg-atas{margin: 10px 0;overflow: hidden;}
        .head {float:right;margin-right:12%;}
            .head h3{color: #f0f0f0;}
            .head h5{color: #f0f0f0;}
        .logo{width: 90px;}
        .footer{position:absolute;bottom:0;width:100%;text-align: center;background-color: black;padding-top: 9px;}
        .footer p{font-size: 10px;line-height:15px;color: darkgrey;}
        .info-news p{font-size: 10px;color: #333333;}
        .info-news img{margin: 10px 0 20px 0;}
        .info-p {display: block;padding: 10px;background-color: rgba(0, 0, 0, .5);}
        .info-p p{font-size: 13px;color: #dedbdb;}
        .spans {width: 220px;margin-right: 5px;}
        .hd-btm {text-align: center;padding-top: 10px;}
        /*#img-news {overflow:hidden;width: 400px;margin:auto;text-align: center;background-color: red;}*/
    </style>

    <title>Agenda Ruang Rapat</title>
</head>

<?php
    //query agenda rapat
    $agd = "SELECT A . * , (SELECT COUNT( room_name ) FROM todays_v2 WHERE room_name = A.room_name) AS jumlah FROM todays_v2 A";
    $agd_data = mysql_query($agd,$db2);
    while ($row_agd = mysql_fetch_array($agd_data)){$hasil[] = $row_agd;}
    
?>
<body class="metro" style="width:100%;">
    <div class="container">
        <?php
            $array_hr= array(1=>"Senin","Selasa","Rabu","Kamis","Jumat","Sabtu","Minggu");
            $hr = $array_hr[date('N')];
            $tgl= date('j');
            $array_bln = array(1=>"Januari","Februari","Maret", "April", "Mei","Juni","Juli","Agustus","September","Oktober", "November","Desember");
            $bln = $array_bln[date('n')];
            $thn = date('Y');
        ?>
        <div class="grid" style="margin: 0 auto;width: 1366px;">
            <div class="row">
            <div style="margin: 0 10px;width:900px;float:left;">
            <div style="overflow:hidden;">
                <div class="head">
                    <h3>AGENDA HARI INI</h3>
                    <h5><?php echo $hr.", ".$tgl." ".$bln." ".$thn."";?></h5>
                </div>
                <div id="lg-atas" style="float:right;">
                    <!-- <div class="logo"><img src="assets/img/logo_biro_umum.png"></div> -->
                </div>
                <div id="lg-atas" style="float:left;">
                    <div class="times inverse" data-role="times" data-blink="false" style="text-align:left"></div>
                </div>
            </div>
            
        
                <div class="example" style="height:360px;">
                    <table class="table" style="background-color: rgba(255, 255, 255, .6);">
                        <thead>
                        <tr>
                            <th class="text-left" width="300px">Waktu</th>
                            <th class="text-left" width="370px;">Rapat</th>
                            <th class="text-left">PIC</th>
                        </tr>
                        </thead>
                    </table>
                    <marquee class="smooth_m" behavior="scroll" direction="up" onmouseout="this.start()" onmouseover="this.stop()" scrollamount="2" height="280">
                    <table class="table striped">

                        <tbody>
                        <?php
                            $no = 1; $jml = 1;
                            foreach ($hasil as $row_agd){
                                if($row_agd['room_name'] == 'SG-1&2'):
                                    $gedung = '(Gedung Baru Lantai Dasar)';
                                    // $clr = 'bg-darkBlue';
                                elseif($row_agd['room_name'] == 'SG-3'):
                                    $gedung = '(Gedung Baru Lantai Dasar)';
                                    // $clr = 'bg-magenta';
                                elseif($row_agd['room_name'] == 'SG-4'):
                                    $gedung = '(Gedung Baru Lantai Dasar)';
                                    // $clr = 'bg-lightBlue';
                                elseif($row_agd['room_name'] == 'SG-5'):
                                    $gedung = '(Gedung Baru Lantai Dasar)';
                                    // $clr = 'bg-emerald';
                                elseif($row_agd['room_name'] == 'R-203'):
                                    $gedung = '(Gedung Madiun Lantai 3)';
                                    // $clr = 'bg-amber';
                                elseif($row_agd['room_name'] == 'R-204'):
                                    $gedung = '(Gedung Madiun Lantai 4)';
                                    // $clr = 'bg-crimson';
                                elseif($row_agd['room_name'] == 'SS-1&2'):
                                    $gedung = '(Gedung TS 2A Lantai 2)';
                                    // $clr = 'bg-darkBlue';
                                elseif($row_agd['room_name'] == 'SS-3'):
                                    $gedung = '(Gedung TS 2A Lantai 2)';
                                    // $clr = 'bg-magenta';
                                elseif($row_agd['room_name'] == 'SS-4'):
                                    $gedung = '(Gedung TS 2A Lantai 2)';
                                    // $clr = 'bg-lightBlue';
                                elseif($row_agd['room_name'] == 'RR-Bakrie1'):
                                    $gedung = '(Gedung Wisma Bakrie)';
                                    // $clr = 'bg-emerald';
                                elseif($row_agd['room_name'] == 'RR-Bakrie2'):
                                    $gedung = '(Gedung Wisma Bakrie)';
                                    // $clr = 'bg-amber';
                                endif;

                                if ($jml <= 1){
                        ?>
                            <tr class="fg-white bg-darkBlue"><td colspan="2"><i class="icon-location fg-white"></i> <?=$row_agd['room_name'];echo "-";echo $gedung;?><td></tr>
                        <?php
                            $jml = $row_agd['jumlah'];
                            $no++;
                            } else {
                            $jml = $jml - 1;
                            }
                        ?>
                                    <tr><td class="text-left" width="140px;"><?=date('H:i',$row_agd['start_time']);?> s/d <?=date('H:i',$row_agd['end_time']);?></td>
                                        <td class="text-center"><?=$row_agd['name'];?></td>
                                        <td class="text-center">
                                            <div><?=$row_agd['penanggung_jawab'];?></div>
                                            <div><i style="font-size:8px;"><?=$row_agd['telp'];?></i></div>
                                        </td>
                                    </tr>
                        <?php } ?>
                        </tbody>

                        <br>
                    </table>
                    </marquee>
                </div>
                <div style="float:left;padding:0;width:900px;">
                <div class="spans">
                    <div class="example-info">
                    <div class="tile double live" data-role="live-tile" data-effect="slideRight" style="width:100%;height:200px;">
                        <?php if (empty($listhumas)){
                            echo "no data";
                        } else { 
                            foreach ($listhumas as $row_brhm) { ?>
                        <div class="tile-content image image-container" style="width:100%;height:200px;">
                            <img src="../filegambar/<?=$row_brhm['gambar'];?>">
                            <div class="overlay bg-darkRed">
                                <?=$row_brhm['judul'];?>
                            </div>
                        </div>
                        <?php }} ?>
                    </div> 

                    <div class="" style="padding :10px;">
                        <div>
                            <p style="font-color:grey;">
                                Last News<small class="on-right"></small>
                                <a><i class="icon-arrow-right-3 fg-dark"></i></a>
                            </p>
                        </div>
                    <marquee class="smooth_m" behavior="scroll" direction="up" onmouseout="this.start()" onmouseover="this.stop()" scrollamount="3" height="210">
                        <?php if (empty($listhumas)){
                            echo "no data";
                        } else {
                            foreach ($listhumas as $row_brhm) { ?>
                        <div class="info-news">
                            <span class="info-p"><p><?=$row_brhm['judul'];?></p></span>
                            <p><?=$row_brhm['deskripsi'];?></p>
                            <span><img src="../filegambar/<?=$row_brhm['gambar'];?>"></span>
                        </div>
                        <?php }} ?>
                    </marquee>
                        <div class="hd-btm">
                            <span><i class="icon-bookmark-2"></i> Biro Humas</span>
                        </div>
                    </div>

                    </div>
                </div>
                <div class="spans">
                    <div class="example-info">
                    <div class="tile double live" data-role="live-tile" data-effect="slideDown" style="width:100%;height:200px;">
                        <?php if (empty($listumum)){
                            echo "no data";
                        } else { 
                            foreach ($listumum as $row_brumum) { ?>
                        <div class="tile-content image image-container" style="width:100%;height:200px;">
                            <img src="../filegambar/<?=$row_brumum['gambar'];?>">
                            <div class="overlay bg-darkRed">
                                <?=$row_brumum['judul'];?>
                            </div>
                        </div>
                        <?php }} ?>
                    </div>
                    <div class="" style="padding :10px;">
                        <div>
                            <p style="font-color:grey;">
                                Last News<small class="on-right"></small>
                                <a><i class="icon-arrow-right-3 fg-dark"></i></a>
                            </p>
                        </div>
                    <marquee class="smooth_m" behavior="scroll" direction="up" onmouseout="this.start()" onmouseover="this.stop()" scrollamount="3" height="210">
                        <?php if (empty($listumum)){
                            echo "no data";
                        } else {
                            foreach ($listumum as $row_brumum) { ?>
                        <div class="info-news">
                            <span class="info-p"><p><?=$row_brumum['judul'];?></p></span>
                            <p><?=$row_brumum['deskripsi'];?></p>
                            <span><img src="../filegambar/<?=$row_brumum['gambar'];?>"></span>
                        </div>
                        <?php }} ?>
                    </marquee>
                        <div class="hd-btm">
                            <span><i class="icon-bookmark-2"></i> Biro Umum</span>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="spans">
                    <div class="example-info">
                    <div class="tile double live" data-role="live-tile" data-effect="slideUp" style="width:100%;height:200px;">
                        <?php if (empty($listsdm)){
                            echo "no data";
                        } else { 
                            foreach ($listsdm as $row_brsdm) { ?>
                        <div class="tile-content image image-container" style="width:100%;height:200px;">
                            <img src="../filegambar/<?=$row_brsdm['gambar'];?>">
                            <div class="overlay bg-darkRed">
                                <?=$row_brsdm['judul'];?>
                            </div>
                        </div>
                        <?php }} ?>
                    </div>
                    <div class="" style="padding :10px;">
                        <div>
                            <p style="font-color:grey;">
                                Last News<small class="on-right"></small>
                                <a><i class="icon-arrow-right-3 fg-dark"></i></a>
                            </p>
                        </div>
                    <marquee class="smooth_m" behavior="scroll" direction="up" onmouseout="this.start()" onmouseover="this.stop()" scrollamount="3" height="210">
                        <?php if (empty($listsdm)){
                            echo "no data";
                        } else {
                            foreach ($listsdm as $row_brsdm) { ?>
                        <div class="info-news">
                            <span class="info-p"><p><?=$row_brsdm['judul'];?></p></span>
                            <p><?=$row_brsdm['deskripsi'];?></p>
                            <span><img src="../filegambar/<?=$row_brsdm['gambar'];?>"></span>
                        </div>
                        <?php }} ?>
                    </marquee>
                        <div class="hd-btm">
                            <span><i class="icon-bookmark-2"></i> Biro SDM</span>
                        </div>
                    </div>    
                    </div>
                </div>
                <div class="spans">
                    <div class="example-info">
                    <div class="tile double live" data-role="live-tile" data-effect="slideLeft" style="width:100%;height:200px;">
                        <?php if (empty($listpjp)){
                            echo "no data";
                        } else { 
                            foreach ($listpjp as $row_pjp) { ?>
                        <div class="tile-content image image-container" style="width:100%;height:200px;">
                            <img src="../filegambar/<?= $row_pjp['gambar'];?>">
                            <div class="overlay bg-darkRed">
                                <?=$row_pjp['judul'];?>
                            </div>
                        </div>
                        <?php }} ?>
                    </div>
                    <div class="" style="padding :10px;">
                        <div>
                            <p style="font-color:grey;">
                                Last News<small class="on-right"></small>
                                <a><i class="icon-arrow-right-3 fg-dark"></i></a>
                            </p>
                        </div>
                    <marquee class="smooth_m" behavior="scroll" direction="up" onmouseout="this.start()" onmouseover="this.stop()" scrollamount="3" height="210">
                        <?php if (empty($listpjp)){
                            echo "no data";
                        } else {
                            foreach ($listpjp as $row_pjp) { ?>
                        <div class="info-news">
                            <span class="info-p"><p><?=$row_pjp['judul'];?></p></span>
                            <p><?=$row_pjp['deskripsi'];?></p>
                            <span><img src="../filegambar/<?=$row_pjp['gambar'];?>"></span>
                        </div>
                        <?php }} ?>
                    </marquee>
                        <div class="hd-btm">
                            <span><i class="icon-bookmark-2"></i> Koperasi PJP</span>
                        </div>
                    </div>
                    </div>
                </div>
                
            </div>
            </div>
            
            <div style="float:left;padding:0 5px;width:440px">
<!--                <div style="margin:20px 10px 24px 290px;"><button class="button" id="createFlatWindow">login</button> | <i class="icon-user" style="background: black;color: white;padding: 5px;border-radius: 50%"></i></div>-->
                <div style="margin:20px 10px 24px 350px;"><a href="#"> | <i class="icon-user" style="background: black;color: white;padding: 5px;"></i></a></div>

                
                    <div class="example-info">
                    <div style="padding :10px;">
                        <div>
                            <p style="font-color:grey;">
                                Last News<small class="on-right"></small>
                                <a><i class="icon-arrow-right-3 fg-dark"></i></a>
                            </p>
                        </div>
                    <marquee class="smooth_m" behavior="scroll" direction="up" onmouseout="this.start()" onmouseover="this.stop()" scrollamount="3" height="810">
                        <?php if (empty($listall)){
                            echo "no data";
                        } else {
                            foreach ($listall as $row_all) { ?>
                        <div class="info-news">
                            <span class="info-p"><p><?=$row_all['judul'];?></p></span>
                            <p><?=$row_all['deskripsi'];?></p>
                            <span id='img-news'><img src="../filegambar/<?=$row_all['gambar'];?>"></span>
                        </div>
                        <br>
                        <?php }} ?>
                    </marquee>
                    </div>

                    </div>
                <!-- </div> -->
            </div>

        </div>
        </div>

        <div class="footer">
            <p>
                Developed by Pusdatinrenbang |
                http://agenda.bappenas.go.id |
                copyright &copy; 2015 All Rights Reserved</p>
        </div>
    </div> <!-- End of container -->

    <script src="<?=base_url();?>style/docs/js/hitua.js"></script>
    <script type="text/javascript">
        $('.bxslider').bxSlider({
          // mode: 'vertical',
          auto: true,
          autoControls: true,
          adaptiveHeight: true,
          captions: true
        });
        
        });        
    </script>
</body>
</html>