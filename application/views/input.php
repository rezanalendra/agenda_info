<?php $this->load->view('header');?>

<style type="text/css">
    #nav-menu{
        max-height: 650px;
        overflow-y: scroll;
    }
</style>

<body class="metro">
    <header class="bg-dark"><?php $this->load->view('menu');?></header>
    <div class="container">
                <div class="grid">
                    <div class="row">
                        <div class="span11">
                            <div class="example">
                            <?php if (is_null($edit)) {
                                $action = "index.php/main/add_user";
                            }else{
                                $action = "index.php/main/update_user";
                            }?>
                                <form method="post" enctype="multipart/form-data" action="<?php echo base_url(); echo $action;?>">
                                    <fieldset>
                                    <?php if (is_null($edit)) {
                                        $judul = "";
                                        $deskripsi = "";
                                        $legend = "Tambah Data User";
                                    }else{
                                        $legend = "Edit Data User";
                                        $username = $edit->username;
                                        $password = $edit->password;
                                        echo form_hidden('id',$edit->id);
                                        echo form_hidden('role',$edit->role);
                                    }?>

                                        <legend><?php echo $legend;?></legend>
                                        <label>User Name</label>
                                        <div class="input-control text" data-role="input-control">
                                            <input type="text" name="username" value="<?php echo $username;?>">
                                        </div>
                                        <label>Password</label>
                                        <div class="input-control text" data-role="input-control">
                                            <input type="password" name="password" value="">
                                        </div>
                                        <br>
                                        <input type="hidden" name="pwd" value="<?=$password;?>">
                                        <input type="submit" name="submit" value="Submit">
                                        <input type="reset" name="reset" value="Reset" onClick="window.location.reload()">
                                        <div style="margin-top: 20px">
                                        </div>
                                    </fieldset>
                                </form>
                            </div>
                    <div class="span">
                            <div class="example">
                                <legend>List Data User</legend>
                                <table class="table bordered hovered">
                                    <thead>
                                    <tr>
                                        <th class="text-left">No</th>
                                        <th class="text-left">Nama User</th>
                                        <th class="text-left">Password</th>
                                        <!-- <th class="text-left">Role</th> -->
                                        <th class="text-left">action</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    <?php $no = 1;
                                    if (empty($userlist)){
                                        echo "no data";
                                    } else {
                                        foreach ($userlist as $user):
                                    ?>
                                    <tr class="">
                                        <td><?=$no++;?></td>
                                        <td class="right"><?php echo $user->username;?></td>
                                        <td class="right"><?php echo $user->password;?></td>
                                        <!-- <td class="right"><?php echo $user->role;?></td> -->
                                        <td class="right">
                                        <a href="<?=base_url();?>index.php/main/edit_user/<?php echo $user->id;?>" class="button info mini"><span class="icon-pencil"></span> View</a>
                                        <a href="<?=base_url();?>index.php/main/delete_user/<?php echo $user->id;?>" class="button danger mini"><span class="icon-cancel"></span> Delete</a>
                                        </td>
                                    </tr>
                                        <script type="text/javascript">
                                            function confirm_x(){
                                                var c = confirm("Anda Yakin Ingin Menghapus Berita?<?=$user->id;?>");
                                                if (c){
                                                    window.location="delete_user/<?=$user->id;?>";
                                                } else {
                                                    window.location="tambah";
                                                }
                                          }
                                        </script>
                                    <?php endforeach; } ?>
                                    </tbody>
                                </table>

                            </div>
                        </div>

                        </div>
                    </div>
                </div>
    </div>
</body>
</html>