<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="product" content="Metro UI CSS Framework">
    <meta name="description" content="Simple responsive css framework">
    <meta name="author" content="RezaBuyungNalendra">

    <link href="<?=base_url();?>assets/docs/css/iconFont.css" rel="stylesheet">
    <link href="<?=base_url();?>assets/docs/css/metro-bootstrap-responsive.css" rel="stylesheet">
    <link href="<?=base_url();?>assets/docs/css/docs.css" rel="stylesheet">
    <link href="<?=base_url();?>assets/docs/js/prettify/prettify.css" rel="stylesheet">
    <link href="<?=base_url();?>assets/bxslider/jquery.bxslider.css" rel="stylesheet">

    <!-- Load JavaScript Libraries -->
    <script src="<?=base_url();?>assets/docs/js/jquery/jquery.min.js"></script>
    <script src="<?=base_url();?>assets/docs/js/jquery/jquery.widget.min.js"></script>
    <script src="<?=base_url();?>assets/docs/js/jquery/jquery.mousewheel.js"></script>
    <script src="<?=base_url();?>assets/docs/js/prettify/prettify.js"></script>
    <script src="<?=base_url();?>assets/docs/js/holder/holder.js"></script>
    <script src="<?=base_url();?>assets/bxslider/jquery.bxslider.min.js"></script>
    
    <!-- Metro UI CSS JavaScript plugins -->
    <script src="<?=base_url();?>assets/docs/js/load-metro.js"></script>

    <!-- Local JavaScript -->
    <script src="<?=base_url();?>assets/docs/js/docs.js"></script>
    <script src="<?=base_url();?>assets/docs/js/github.info.js"></script>
    <script src="<?=base_url();?>assets/docs/js/marquee.js"></script>
    <link href="<?=base_url();?>assets/docs/css/metro-bootstrap.css" rel="stylesheet">
    <title>Info Manager</title>
</head>