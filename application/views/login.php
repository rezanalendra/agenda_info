<?php $this->load->view('header');?>

<body class="metro">
    <header class="bg-dark"><?php $this->load->view('menu');?></header>
    <div class="container">
                <div class="grid">
                    <div class="row">
                        <div class="span9">
                            <h2 id="_default"><i class="icon-accessibility on-left"></i>Login Page</h2>
                            <div class="example">
                                <form method="post" enctype="multipart/form-data" action="<?=base_url();?>index.php/login/" >
                                    <fieldset>
                                        <?php if ($this->uri->segment(3) == 'error'){ ?>
                                                <div class="balloon up">
                                                    <div class="padding20">
                                                        <p class="fg-magenta"><span class="icon-cancel"></span> Cek Login Anda</p>
                                                    </div>
                                                </div>
                                        <?php } else if ($this->uri->segment(3) == 'logout') { ?>
                                                <div class="balloon up">
                                                    <div class="padding20">
                                                        <p class="fg-teal"><span class="icon-checkmark"></span> Berhasil Logout</p>
                                                    </div>
                                                </div>
                                        <?php } ?>
                                        <label>Username</label>
                                        <div class="input-control text" data-role="input-control">
                                            <input type="text" placeholder="type username" name="username" required>
                                            <!-- <button class="btn-clear" tabindex="-1"></button> -->
                                        </div>
                                        <label>Password</label>
                                        <div class="input-control password" data-role="input-control">
                                            <input type="password" placeholder="type password" name="password" required>
                                            <!-- <button class="btn-reveal" tabindex="-1"></button> -->
                                        </div>
                                        <button type="submit" name="submit">Silahkan Login</button>
                                    </fieldset>
                                </form>
                            </div>
                            <h6>
                                <i>Aplikasi Untuk Penginputan Pengumuman</i>
                            </h6>
                        </div>
                    </div>
                </div>
    </div>
</body>
</html>