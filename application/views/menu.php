<div class="navigation-bar dark">
    <div class="navigation-bar-content container">
        <a href="<?=base_url();?>index.php/main/" class="element"><span class="icon-grid-view"></span>  ADMIN AGENDA <sup>2.0</sup></a>
        <span class="element-divider"></span>
        <a href="<?=base_url();?>acara_hari_ini.php" target="_blank" class="element"> Info AGENDA </a>
        <a class="element1 pull-menu" href="#"></a>
        

        <div class="no-tablet-portrait no-phone">
            <span class="element-divider place-right"></span>
            <a title="Logout" href="<?=base_url();?>index.php/login/logout" class="element place-right"><span class="icon-switch"></span> </a>
            <span class="element-divider place-right"></span>
            <?php if ($this->session->userdata('role') == '1'){ ?>
            <a title="add user" href="<?=base_url();?>index.php/main/tambah" class="element place-right"><span class="icon-plus"></span> USER</a>
            <span class="element-divider place-right"></span>
            <?php } ?>
            <!--<a title="CSS3 validator" href="http://jigsaw.w3.org/css-validator/validator?uri=metroui.org.ua&profile=css3&usermedium=all&warning=no&vextwarning=" class="element place-right"><span class="icon-css3"></span></a>
            <span class="element-divider place-right"></span>
            <div class="element place-right" title="GitHub Stars"><span class="icon-star"></span> <span class="github-watchers">0</span></div>
            <!--<span class="element-divider place-right"></span>-->
            <!--<div class="element place-right" title="GitHub Forks"><span class="icon-share-2"></span> <span class="github-forks">0</span></div>-->
        </div>
    </div>
</div>
