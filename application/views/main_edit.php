<?php $this->load->view('header');?>

<body class="metro">
    <header class="bg-dark"><?php $this->load->view('menu');?></header>
    <div class="container">
                <div class="grid">
                    <div class="row">
                    
                    
                        <div class="span3">
                            <h2 id="_default"><i class="icon-accessibility on-left"></i>Main Page</h2>
                            <div class="example" id="nav-menu">
                            <?php 
                            $role = $this->session->userdata('role');
                            if ($role == '1'){
                                $list = $listuser;
                            } else {
                                $list = $listuser_by;
                            }
                            if (empty($list)){
                                echo "nodata";
                            } foreach ($list as $user){
                                echo "
                                    <a href='#'>
                                    <div class='tile bg-darkPink'>
                                        <div class='tile-content icon'>
                                            <i class='icon-cc-by'></i>
                                        </div>
                                        <div class='tile-status'>
                                            <span class='name'> ".$user['username']." </span>
                                        </div> 
                                    </div>
                                    </a>       
                                    ";
                            } ?>
                            </div>
                        </div>

                       
                        <div class="span11">
                            <h2 id="_default">welcome, <?=strtoupper($this->session->userdata('username'));?></h2>
                            <div class="example">
                                <?php foreach ($rowData as $row): ?>
                                <form method="post" enctype="multipart/form-data" action="<?=base_url();?>index.php/main/edit/">
                                    <fieldset>
                                        <legend>Edit Data Berita</legend>
                                        <label>Lama Tayang</label>
                                        <div class="input-control select" data-role="input-control">
                                            <select name="tayang">
                                            <?php for($hari = 1; $hari <= 7; $hari++){
                                                echo "<option value='$hari;'>$hari hari</option>";
                                            }?>
                                            </select>
                                        </div>
                                        <label>Judul Berita</label>
                                        <div class="input-control text" data-role="input-control">
                                            <input type="text" name="judul_brt" value="<?=$row['judul'];?>">
                                            <!-- <button class="btn-clear" tabindex="-1"></button> -->
                                        </div>
                                        <label>Deskripsi Berita</label>
                                        <div class="input-control textarea" data-role="input-control">
                                            <textarea rows="10" name="deskripsi_brt"><?=$row['deskripsi'];?></textarea>
                                        </div>
                                        <label>Upload Gambar</label>
                                        <div class="input-control" data-role="input-control">
                                            <input type="file" name="attachment">
                                            <?php 
                                                if ($row['gambar'] != ''){
                                                    echo "<br>";
                                                    echo "<input type='hidden' name='attachment' value='$row[gambar]'>";
                                                    echo "<img style='width:250px;' src='".base_url()."/filegambar/".$row['gambar']."'>";
                                                    echo "<br>";
                                                    echo $row['gambar'];
                                                } else {
                                                    echo "no image";
                                                }
                                            ?>
                                        </div>
                                        <br>
                                        

                                        <input type="hidden" name="id" value="<?=$row['id'];?>">
                                        <input type="hidden" name="owner" value="<?=$row['owner'];?>">
                                        <input type="submit" name="edit" value="Edit">
                                        <!-- <input type="reset" name="reset" value="Reset" onClick="window.location.reload()"> -->
                                        <div style="margin-top: 20px">
                                        </div>

                                    </fieldset>
                                </form>
                            <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                    
                    
                        
                </div>
    </div>
</body>
</html>