<?php $this->load->view('header');?>
<style type="text/css">
    #nav-menu{
        max-height: 650px;
        overflow-y: scroll;
    }
</style>
<body class="metro">
    <header class="bg-dark"><?php $this->load->view('menu');?></header>
    <div class="container">
                <div class="grid">
                    <div class="row">
                    
                    
                        <div class="span3">
                            <h2 id="_default"><i class="icon-accessibility on-left"></i>Main Page</h2>
                            <div class="example" id="nav-menu">
                            <?php 
                            $role = $this->session->userdata('role');
                            if ($role == '1'){
                                $list = $listuser;
                            } else {
                                $list = $listuser_by;
                            }
                            if (empty($list)){
                                echo "nodata";
                            } foreach ($list as $user){
                                echo "
                                    <a href='#'>
                                    <div class='tile bg-darkPink'>
                                        <div class='tile-content icon'>
                                            <i class='icon-cc-by'></i>
                                        </div>
                                        <div class='tile-status'>
                                            <span class='name'> ".$user['username']." </span>
                                        </div> 
                                    </div>
                                    </a>       
                                    ";
                            } ?>
                            </div>
                        </div>

                        <div class="span11">
                            <h2 id="_default">welcome, <?=strtoupper($this->session->userdata('username'));?></h2>
                            <div class="example">
                                
                                <form method="post" enctype="multipart/form-data" action="<?=base_url();?>index.php/main/">
                                    <fieldset>
                                        <?php if ($this->uri->segment(3) == 'success'){ ?>
                                                <div class="balloon up">
                                                    <div class="padding20">
                                                        <p class="fg-green"><span class="icon-checkmark"></span> Berhasil Input Data</p>
                                                    </div>
                                                </div>
                                        <?php } else if ($this->uri->segment(3) == 'delete_success'){  ?>  
                                            <div class="balloon up">
                                                    <div class="padding20">
                                                        <p class="fg-green"><span class="icon-checkmark"></span> Berhasil Hapus Data</p>
                                                    </div>
                                                </div>
                                        <?php } ?>      
                                        <legend>Input Data Berita</legend>
                                        <label>Lama Tayang</label>
                                        <div class="input-control select" data-role="input-control">
                                            <select name="tayang">
                                            <?php for($hari = 1; $hari <= 7; $hari++){
                                                echo "<option value='$hari;'>$hari hari</option>";
                                            } ?>
                                            </select>
                                        </div>
                                        <label>Judul Berita</label>
                                        <div class="input-control text" data-role="input-control">
                                            <input type="text" name="judul_brt">
                                            <!-- <button class="btn-clear" tabindex="-1"></button> -->
                                        </div>
                                        <label>Deskripsi Berita</label>
                                        <div class="input-control textarea" data-role="input-control">
                                            <textarea rows="10" name="deskripsi_brt"></textarea>
                                        </div>
                                        <label>Upload Gambar</label>
                                        <div class="input-control" data-role="input-control">
                                            <input type="file" name="attachment" value="0980980">
                                        </div>
                                        <br>
                                        

                                        <input type="submit" name="submit" value="Submit">
                                        <input type="reset" name="reset" value="Reset" onClick="window.location.reload()">
                                        <div style="margin-top: 20px">
                                        </div>

                                    </fieldset>
                                </form>

                            </div>
                            
                        </div>
                    </div>
                    <div class="span">
                            <div class="example">
                                <legend>List Data Berita</legend>
                                <table class="table bordered hovered">
                                    <thead>
                                    <tr>
                                        <th class="text-left">No</th>
                                        <th class="text-left">Judul Berita</th>
                                        <th class="text-left">Deskripsi</th>
                                        <th class="text-left">Gambar</th>
                                        <th class="text-left">action</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    <?php $no = 1;
                                    if (empty($listdata)){
                                        echo "no data";
                                    } else {
                                        foreach ($listdata as $key):
                                    ?>
                                    <tr class="">
                                        <td><?=$no++;?></td>
                                        <td class="right"><?=$key['judul'];?></td>
                                        <td class="right">
                                            <?=$key['deskripsi'];?>
                                        </td>
                                        <td><img style="width:100px" src="<?=base_url();?>/filegambar/<?=$key['gambar'];?>"></td>
                                        <td class="right">
                                        <a href="<?=base_url();?>index.php/main/edit/<?=$key['id'];?>" class="button info mini"><span class="icon-pencil"></span> view</a>
                                        <a href="<?=base_url();?>index.php/main/delete/<?=$key['id'];?>" class="button danger mini"><span class="icon-cancel"></span> delete</a>
                                        <!-- <a href="#" onclick="show_confirm()" class="button danger mini"><span class="icon-cancel"></span> delete</a> -->
                                        </td>
                                    </tr>
                                    <script type="text/javascript">
                                        function show_confirm(){
                                            var c = confirm("Anda Yakin Ingin Menghapus Berita? <?=$key['id'];?>");
                                            if (c){
                                                window.location="<?=base_url();?>index.php/main/delete/<?=$key['id'];?>";
                                            } else {
                                                window.location="<?=base_url();?>index.php/main/";
                                            }
                                      }
                                    </script>
                                    <?php endforeach; } ?>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                </div>
    </div>
    
</body>
</html>