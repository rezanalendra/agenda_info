<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class modEditdata extends CI_Model {

    function editdata()
    {
        $judul_brt = $_POST['judul_brt'];
        $deskripsi_brt = $_POST['deskripsi_brt'];
        // $owner = $this->session->userdata('username');
        $tayang = $_POST['tayang'];
        $y = date('Y'); $m = date('m'); $d = date('d')+$tayang;
        $time = mktime(0, 0, 0, $m, $d, $y);
        $date = date('Y-m-d',$time);
        $owner = $_POST['owner'];
        $gbr_org = $_FILES['attachment']['name'];
        if ($gbr_org == ''){
            $gbr = $_POST['attachment'];
            $file = $gbr;
        } else {
            $gbr = $_FILES['attachment']['name'];
            $file = "".date('Ym-his')."".$gbr."";
        }
        
        $id = $_POST['id'];
        //upload file gambar
        $config['upload_path'] = './filegambar/';
        $config['allowed_types'] = 'jpg|png|jpeg|gif';
        $config['overwrite'] = FALSE;
        $config['max_size'] = '10000';
        $config['file_name'] = $file;
        $config['remove_spaces'] = TRUE;
        /* Load the upload library */
        if ($gbr_org != ''){
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('attachment'))
        {
            // echo $this->data['errors'] = $this->upload->display_errors();
            // var_dump($config);
            // exit();
        } else {
            $this->data['upload_data'] = $this->upload->data();
        }} 
        
        $data = array(
            'judul' => $judul_brt,
            'deskripsi' => $deskripsi_brt,
            'gambar' => $file,
            'owner' => $owner,
            'tayang' => $time
            );
        $this->db->where('id',$id);
        $this->db->update('tbl_news',$data);
    }

    function viewdata($id)
    {
        // $owner = $this->session->userdata('username');
        $this->db->select('*');
        $this->db->from('tbl_news');
        // $this->db->where('owner',$owner);
        $this->db->where('id',$id);
        $getData = $this->db->get();

        if($getData->num_rows() > 0)
        {
            $rowData = $getData->result_array();
            return $rowData;
        } else {
            return null;
        }
    }

    function deletedata($id)
    {
        // $id = $this->uri->segment(3);
        $this->db->where('id',$id);
        $this->db->delete('tbl_news');
    }
}

    