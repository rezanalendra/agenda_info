<?php if ( ! defined('BASEPATH'))
		exit('No direct script access allowed');

class modelUser extends CI_Model{
    //Memperoleh Data Informasi Sebelum di Edit
    function get_user()
    {
        $query=$this->db->query("SELECT * FROM tbl_login WHERE role='2' ORDER BY id DESC");
        return $query->result();
    }

    function get_edit_user($id){
       return $this->db->get_where("tbl_login", array("id" => $id))->row();
    }

    function edit_user($id, $update){
        $this->db->WHERE('id', $id);
        $this->db->update('tbl_login', $update);
    }

    function delete($id){
        $query=$this->db->query("DELETE FROM tbl_login WHERE id='$id'");
    }
}