<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class modInputdata extends CI_Model {

    function insertdata()
    {
        $judul_brt = $_POST['judul_brt'];
        $deskripsi_brt = $_POST['deskripsi_brt'];
        $owner = $this->session->userdata('username');
        $gbr = $_FILES['attachment']['name'];
        $file = "".date('Ym-his')."".$gbr."";
        $tayang = $_POST['tayang'];
        $y = date('Y'); $m = date('m'); $d = date('d')+$tayang;
        $time = mktime(0, 0, 0, $m, $d, $y);
        $date = date('Y-m-d',$time);
        // echo $time;
        // echo "<br>";
        // echo date('Y-m-d',$time);
        // exit();
        //upload file gambar
        $config['upload_path'] = './filegambar/';
        $config['allowed_types'] = 'jpg|png|jpeg|gif';
        $config['overwrite'] = FALSE;
        $config['max_size'] = '10000';
        $config['file_name'] = $file;
        $config['remove_spaces'] = TRUE;
        // $config['encrypt_name'] = TRUE;
        /* Load the upload library */
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('attachment'))
        {
            echo $this->data['errors'] = $this->upload->display_errors();
            die();
        } else {
            $this->upload->data();
        }
        
        $data = array(
            'judul' => $judul_brt,
            'deskripsi' => $deskripsi_brt,
            'gambar' => $file,
            'owner' => $owner,
            'tayang' => $time
            );
        $this->db->insert('tbl_news',$data);
    }

    function add_user()
    {
        $data = array(
            'username' => $_POST['username'],
            'password' => md5($_POST['password']),
            'role' => '2'
            );
        $this->db->insert('tbl_login',$data);
    }

}

    