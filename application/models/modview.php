<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class modView extends CI_Model {

	function listdata()
	{
		$owner = $this->session->userdata('username');
		if ($owner == 'administrator'){
			$this->db->select('*');
			$this->db->from('tbl_news');
			$this->db->order_by('date',DESC);
		} else {
			$this->db->select('*');
			$this->db->from('tbl_news');
			$this->db->where('owner',$owner);
			$this->db->order_by('date',DESC);
		}
		$getData = $this->db->get();

		if ($getData->num_rows() > 0){
			$listdata = $getData->result_array();
			return $listdata;
		} else {
			return null;
		}
	}

	function listhumas()
	{
		$this->db->select('*');
		$this->db->from('tbl_news');
		$this->db->where('owner','biro_humas');
		$this->db->order_by('date',DESC);
		$getData = $this->db->get();

		if ($getData->num_rows() > 0){
			$listhumas = $getData->result_array();
			return $listhumas;
		} else {
			return null;
		}
	}

	function listsdm()
	{
		$this->db->select('*');
		$this->db->from('tbl_news');
		$this->db->where('owner','biro_sdm');
		$this->db->order_by('date',DESC);
		$getData = $this->db->get();

		if ($getData->num_rows() > 0){
			$listsdm = $getData->result_array();
			return $listsdm;
		} else {
			return null;
		}
	}

	function listumum()
	{
		$this->db->select('*');
		$this->db->from('tbl_news');
		$this->db->where('owner','biro_umum');
		$this->db->order_by('date',DESC);
		$getData = $this->db->get();

		if ($getData->num_rows() > 0){
			$listumum = $getData->result_array();
			return $listumum;
		} else {
			return null;
		}
	}

	function listpjp()
	{
		$this->db->select('*');
		$this->db->from('tbl_news');
		$this->db->where('owner','koperasi_pjp');
		$this->db->order_by('date',DESC);
		$getData = $this->db->get();

		if ($getData->num_rows() > 0){
			$listpjp = $getData->result_array();
			return $listpjp;
		} else {
			return null;
		}
	}

	function listall()
	{
		$this->db->select('*');
		$this->db->from('tbl_news');
		// $this->db->where('owner','2');
		$this->db->order_by('date',DESC);
		$getData = $this->db->get();

		if ($getData->num_rows() > 0){
			$listall = $getData->result_array();
			return $listall;
		} else {
			return null;
		}
	}

	function listuser()
	{
		$this->db->select('*');
		$this->db->from('tbl_login');
		$this->db->where('role','2');
		$this->db->order_by('id',DESC);
		$getData = $this->db->get();

		if ($getData->num_rows() > 0){
			$listuser = $getData->result_array();
			return $listuser;
		} else {
			return null;
		}
	}

	function listuser_by()
	{
		$owner = $this->session->userdata('username');
		$this->db->select('*');
		$this->db->from('tbl_login');
		$this->db->where('username',$owner);
		$getData = $this->db->get();

		if ($getData->num_rows() > 0){
			$listuser_by = $getData->result_array();
			return $listuser_by;
		} else {
			return null;
		}
	}
}